class SliderModel {
  String imageAssetPath;
  String title;
  String desc;

  SliderModel({this.imageAssetPath, this.title, this.desc});

  void setImageAssetPath(String getImageAssetPath) {
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setDesc(String getDesc) {
    desc = getDesc;
  }

  String getImageAssetPath() {
    return imageAssetPath;
  }

  String getTitle() {
    return title;
  }

  String getDesc() {
    return desc;
  }
}

List<SliderModel> getSlides() {
  List<SliderModel> slides = new List<SliderModel>();
  SliderModel slider = new SliderModel();

  //1
  slider.setDesc(
      "onBoarding.slide1.description");
  slider.setTitle("onBoarding.slide1.title");
  slider.setImageAssetPath("assets/on_boarding/slide1.png");
  slides.add(slider);

  slider = new SliderModel();

  //2
  slider.setDesc(
      "onBoarding.slide2.description");
  slider.setTitle("onBoarding.slide2.title");
  slider.setImageAssetPath("assets/on_boarding/slide2.png");
  slides.add(slider);

  slider = new SliderModel();

  //3
  slider.setDesc(
      "onBoarding.slide3.description");
  slider.setTitle("onBoarding.slide3.title");
  slider.setImageAssetPath("assets/on_boarding/slide3.png");
  slides.add(slider);

  slider = new SliderModel();

  return slides;
}
