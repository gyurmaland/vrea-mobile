import 'package:flutter/material.dart';

class Worker {
  final int id;
  final String name;

  const Worker({
    @required this.id,
    @required this.name,
  });

  factory Worker.fromJson(Map<String, dynamic> json) => Worker(
        id: json['id'],
        name: json['name'],
      );
}
