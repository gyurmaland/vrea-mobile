class IntervalsResponse {
  int page;
  int totalResults;
  int totalPages;
  List<IntervalModel> results;

  IntervalsResponse(
      {this.page, this.totalResults, this.totalPages, this.results});

  IntervalsResponse.fromJson(List<dynamic> json) {
    results = new List<IntervalModel>();
    json.forEach((v) {
      results.add(new IntervalModel.fromJson(v));
    });
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_results'] = this.totalResults;
    data['total_pages'] = this.totalPages;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class IntervalModel {
  int id;
  String start;
  String end;
  String intervalStatus;

  IntervalModel(this.id, this.start, this.end, this.intervalStatus);

  IntervalModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    start = json['start'];
    end = json['end'];
    intervalStatus = json['intervalStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['start'] = this.start;
    data['end'] = this.end;
    data['intervalStatus'] = this.intervalStatus;
    return data;
  }
}
