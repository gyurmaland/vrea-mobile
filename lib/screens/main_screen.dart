import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';

import 'package:vrea/components/home.dart';
import 'package:vrea/components/notifications.dart';
import 'package:vrea/components/profile/profile.dart';
import 'package:vrea/components/workers.dart';

class MainScreen extends StatefulWidget {
  static const routeName = '/main';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: IndexedStack(
        index: _selectedBottomNavIndex,
        children: _pages,
      ),
      bottomNavigationBar: buildBottomNavigationBar(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      actions: <Widget>[],
    );
  }

  int _selectedBottomNavIndex = 0;

  List<Widget> _pages = [
    Home(),
    Workers(),
    Notifications(),
    Profile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedBottomNavIndex = index;
    });
  }

  BottomNavigationBar buildBottomNavigationBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('bottomNavigationBar.home').tr(),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.work),
          title: Text('bottomNavigationBar.workers').tr(),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notifications),
          title: Text('bottomNavigationBar.notifications').tr(),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.account_circle),
          title: Text('bottomNavigationBar.profile').tr(),
        ),
      ],
      currentIndex: _selectedBottomNavIndex,
      onTap: _onItemTapped,
    );
  }
}
