import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

import 'package:vrea/components/profile/schedule/schedule_settings.dart';
import 'package:vrea/components/profile/schedule/timeline_stream.dart';
import 'package:vrea/constants.dart';
import 'package:vrea/providers/timeline_search.dart';

class ScheduleSettingsScreen extends StatefulWidget {
  static const routeName = '/schedule-settings';

  @override
  _ScheduleSettingsScreenState createState() => _ScheduleSettingsScreenState();
}

class _ScheduleSettingsScreenState extends State<ScheduleSettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: kDefaultPadding,
                  vertical: 15,
                ),
                child: Text(
                  'profile.profile.settings.schedule.title',
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      .copyWith(fontWeight: FontWeight.bold),
                ).tr(),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: kDefaultPadding,
                  vertical: 15,
                ),
                child: Icon(Icons.add),
              ),
            ],
          ),
          ScheduleSettings(),
          Consumer<TimeLineSearch>(
            builder: (ctx, timelineSearchData, _) =>
                TimelineStream(timelineSearchData.getTimeLineWorkDay()),
          ),
        ],
      ),
    );
  }
}
