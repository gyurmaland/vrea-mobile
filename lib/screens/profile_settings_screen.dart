import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:vrea/components/profile/profile_actions.dart';

import 'package:vrea/providers/user.dart' show User;
import 'package:vrea/constants.dart';

class ProfileSettingsScreen extends StatelessWidget {
  static const routeName = '/profile-settings';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: kDefaultPadding,
                vertical: 15,
              ),
              child: Consumer<User>(
                builder: (ctx, userData, child) => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          userData.user.name,
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 30,
                          backgroundImage: NetworkImage(
                            userData.user.imageUrl,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: kDefaultPadding,
                vertical: 15,
              ),
              child: ProfileActions(),
            )
          ],
        ),
      ),
    );
  }
}
