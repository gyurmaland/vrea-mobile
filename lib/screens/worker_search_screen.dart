import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';

import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:vrea/constants.dart';
import 'package:vrea/providers/worker_search.dart';

class WorkerSearchScreen extends StatelessWidget {
  static const routeName = '/worker-search';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: kDefaultPadding,
                vertical: 15,
              ),
              child: Text(
                'workers.search.advanced.title',
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(fontWeight: FontWeight.bold),
              ).tr(),
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: kDefaultPadding,
                    vertical: 15,
                  ),
                  child: Row(
                    children: <Widget>[
                      Text('workers.search.advanced.workDay').tr(),
                      IconButton(
                        icon: Icon(
                          Icons.calendar_today,
                        ),
                        color: kTextColor,
                        onPressed: () async {
                          DateTime newDateTime = await showRoundedDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(DateTime.now().year - 1),
                            lastDate: DateTime(DateTime.now().year + 1),
                            borderRadius: 16,
                            theme: Theme.of(context),
                          );
                          if (newDateTime != null) {
                            Provider.of<WorkerSearch>(context, listen: false)
                                .updateWorkDateTime(newDateTime);
                          }
                        },
                      ),
                      Consumer<WorkerSearch>(
                        builder: (ctx, searchData, child) =>
                            Text(searchData.workDateTime.toString()),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
