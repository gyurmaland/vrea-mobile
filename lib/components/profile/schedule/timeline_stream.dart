import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';

import 'package:vrea/bloc/interval_bloc.dart';
import 'package:vrea/components/profile/schedule/timeline.dart';
import 'package:vrea/models/interval.dart';
import 'package:vrea/service/api_response.dart';

class TimelineStream extends StatefulWidget {
  String workDay;

  TimelineStream(this.workDay);

  @override
  _TimelineStreamState createState() => _TimelineStreamState();
}

class _TimelineStreamState extends State<TimelineStream> {
  IntervalBloc _bloc;

  @override
  void initState() {
    widget.workDay = DateFormat('yyyy-MM-dd').format(DateTime.now());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _bloc = IntervalBloc(widget.workDay);

    return Expanded(
      child: StreamBuilder<ApiResponse<List<IntervalModel>>>(
        stream: _bloc.intervalListStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            switch (snapshot.data.status) {
              case Status.LOADING:
                return Text('loading');
                break;
              case Status.COMPLETED:
                return TimeLine(snapshot.data.data);
                break;
              case Status.ERROR:
                return Text('error');
                break;
            }
          }
          return Container();
        },
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
