import 'package:flutter/material.dart';

import 'package:vrea/models/interval.dart';

class TimeLine extends StatelessWidget {
  final List<IntervalModel> intervalList;

  const TimeLine(this.intervalList);

  @override
  Widget build(BuildContext context) {
    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (OverscrollIndicatorNotification overscroll) {
        overscroll.disallowGlow();
        return;
      },
      child: ListView(
        children: [
          for (int i = 0; i < intervalList.length; i++)
            Container(
              child: Column(
                children: [
                  Text(intervalList[i].start),
                  Text(intervalList[i].end),
                ],
              ),
            ),
        ],
      ),
    );
  }
}
