import 'package:flutter/material.dart';

import 'package:flutter_calendar_week/flutter_calendar_week.dart';
import 'package:provider/provider.dart';

import 'package:vrea/providers/timeline_search.dart';

class ScheduleSettings extends StatefulWidget {
  @override
  _ScheduleSettingsState createState() => _ScheduleSettingsState();
}

class _ScheduleSettingsState extends State<ScheduleSettings> {
  final CalendarWeekController _controller = CalendarWeekController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Colors.black.withOpacity(0.05),
            blurRadius: 10,
            spreadRadius: 1)
      ]),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 5,
        ),
        child: CalendarWeek(
          controller: _controller,
          height: 100,
          minDate: DateTime.now().add(
            Duration(days: -365),
          ),
          maxDate: DateTime.now().add(
            Duration(days: 365),
          ),
          onDatePressed: (DateTime datetime) {
            Provider.of<TimeLineSearch>(context, listen: false)
                .updateTimelineWorkDay(datetime);
          },
          onWeekChanged: () {},
          monthStyle:
              TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
          weekendsStyle:
              TextStyle(color: Colors.black54, fontWeight: FontWeight.w600),
          dayOfWeekStyle:
              TextStyle(color: Colors.black54, fontWeight: FontWeight.w600),
          dayOfWeekAlignment: FractionalOffset.bottomCenter,
          dateStyle:
              TextStyle(color: Colors.black45, fontWeight: FontWeight.w400),
          dateAlignment: FractionalOffset.topCenter,
          todayDateStyle:
              TextStyle(color: Colors.black45, fontWeight: FontWeight.w400),
          todayBackgroundColor: Colors.black.withOpacity(0.15),
          pressedDateBackgroundColor: Theme.of(context).primaryColor,
          pressedDateStyle:
              TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
          dateBackgroundColor: Colors.transparent,
          backgroundColor: Colors.white,
          dayOfWeek: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
          showMonth: true,
          spaceBetweenLabelAndDate: 0,
          dayShapeBorder: CircleBorder(),
          decorations: [
            DecorationItem(
              decorationAlignment: FractionalOffset.bottomRight,
              date: DateTime.now(),
              decoration: Icon(
                Icons.today,
                color: Colors.green,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
