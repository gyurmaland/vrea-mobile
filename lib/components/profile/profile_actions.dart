import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:hexcolor/hexcolor.dart';

import 'package:vrea/components/universal/custom_tile.dart';
import 'package:vrea/helpers/custom_route.dart';
import 'package:vrea/screens/schedule_settings_screen.dart';

class ProfileActions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 10,
            ),
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'profile.profile.title',
                    ).tr(),
                  ],
                )
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Hexcolor('#eaeaea'),
                    ),
                    child: ListView(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      children: <Widget>[
                        CustomTile(
                          'profile.profile.personalDetails',
                          Icon(Icons.person),
                          () {},
                        ),
                        CustomTile(
                          'profile.profile.workLocation',
                          Icon(Icons.map),
                          () {},
                        ),
                        CustomTile(
                          'profile.profile.schedule',
                          Icon(Icons.timelapse),
                          () {
                            Navigator.push(context, SlideRightRoute(page: ScheduleSettingsScreen()));
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
