import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:vrea/constants.dart';
import 'package:vrea/providers/user.dart' show User;
import 'package:vrea/screens/profile_settings_screen.dart';
import 'package:vrea/helpers/custom_route.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: FutureBuilder(
            future: Provider.of<User>(context, listen: false)
                .fetchAndSetProfileData(),
            builder: (ctx, dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapshot.error != null) {
                  return Center(
                    child: Text('An error occurred!'),
                  );
                } else {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Consumer<User>(
                            builder: (ctx, userData, child) => Text(
                              userData.user.name,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 16),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.help,
                                  ),
                                  color: kTextColor,
                                  onPressed: () {},
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 16),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.settings,
                                  ),
                                  color: kTextColor,
                                  onPressed: () {
                                    Navigator.push(context, SlideRightRoute(page: ProfileSettingsScreen()));
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  );
                }
              }
            },
          ),
        ),
      ],
    );
  }
}
