import 'package:flutter/material.dart';

class WorkerListItem extends StatelessWidget {
  final String name;

  WorkerListItem(this.name);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Center(child: Text(this.name)),
    );
  }
}
