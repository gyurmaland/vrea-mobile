import 'package:flutter/material.dart';

import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:provider/provider.dart';

import 'package:vrea/models/worker.dart';
import 'package:vrea/providers/worker_search.dart';
import 'package:vrea/service/worker_remote_api.dart';
import 'package:vrea/components/worker/worker_list_item.dart';

class WorkerListView extends StatefulWidget {
  @override
  _WorkerListViewState createState() => _WorkerListViewState();
}

class _WorkerListViewState extends State<WorkerListView> {
  WorkerListViewDataSource _dataSource;

  @override
  Widget build(BuildContext context) {
    return Consumer<WorkerSearch>(
      builder: (ctx, searchData, _) => PagedSliverList<int, Worker>(
        dataSource: WorkerListViewDataSource(searchData),
        builderDelegate: PagedChildBuilderDelegate<Worker>(
          itemBuilder: (context, worker, index) => WorkerListItem(
            worker.name,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dataSource.dispose();
    super.dispose();
  }
}

class WorkerListViewDataSource extends PagedDataSource<int, Worker> {
  final WorkerSearch workerSearch;

  WorkerListViewDataSource(this.workerSearch) : super(0);
  static const _pageSize = 5;

  Object _activeCallbackIdentity;

  @override
  void fetchItems(int pageKey) {
    WorkerRemoteApi.postWorkerList(pageKey, _pageSize, workerSearch).then((workers) {
      final hasFinished = workers.length < _pageSize;
      final nextPageKey = hasFinished ? null : (pageKey + workers.length);
      notifyNewPage(workers, nextPageKey);
    }).catchError(notifyError);
  }

  void updateWorkerSearchTerm() {
    refresh();
  }

  @override
  void dispose() {
    _activeCallbackIdentity = null;
    super.dispose();
  }
}
