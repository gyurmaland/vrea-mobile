import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';

import 'package:vrea/components/worker/worker_list_view.dart';
import 'package:vrea/components/worker/worker_search_simple.dart';
import 'package:vrea/constants.dart';

class Workers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      "title.workers",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontWeight: FontWeight.bold),
                    ).tr(),
                  ],
                ),
              ],
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: WorkerSearchSimple(),
        ),
        WorkerListView(),
      ],
    );
  }
}
