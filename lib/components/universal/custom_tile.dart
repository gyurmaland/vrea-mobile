import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:hexcolor/hexcolor.dart';

class CustomTile extends StatefulWidget {
  final String title;
  final Icon icon;
  final Function onTileTapUpNav;

  CustomTile(this.title, this.icon, this.onTileTapUpNav);

  @override
  CustomTileState createState() => CustomTileState();
}

class CustomTileState extends State<CustomTile> {
  Color color = Colors.transparent;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: GestureDetector(
        onTapDown: (_) {
          setState(() {
            color = Hexcolor('#DCDCDC');
          });
        },
        onTapCancel: () {
          setState(() {
            color = Colors.transparent;
          });
        },
        onTapUp: (_) {
          setState(() {
            color = Colors.transparent;
          });
          widget.onTileTapUpNav();
        },
        child: ListTile(
          title: Text(widget.title).tr(),
          leading: widget.icon,
        ),
      ),
    );
  }
}
