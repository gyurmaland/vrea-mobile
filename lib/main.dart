import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';

import 'package:vrea/providers/auth.dart';
import 'package:vrea/providers/timeline_search.dart';
import 'package:vrea/providers/user.dart';
import 'package:vrea/providers/worker_search.dart';
import 'package:vrea/screens/login_screen.dart';
import 'package:vrea/screens/main_screen.dart';
import 'package:vrea/screens/on_boarding_screen.dart';
import 'package:vrea/screens/profile_settings_screen.dart';
import 'package:vrea/screens/worker_search_screen.dart';

import 'constants.dart';

void main() {
  runApp(
    EasyLocalization(
      supportedLocales: [Locale('hu', ''), Locale('en', '')],
      path: 'assets/translations',
      fallbackLocale: Locale('hu', ''),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => Auth(),
        ),
        ChangeNotifierProvider(
          create: (_) => WorkerSearch(),
        ),
        ChangeNotifierProxyProvider<Auth, User>(
          update: (ctx, auth, previousUser) => User(),
        ),
        ChangeNotifierProvider(
          create: (_) => TimeLineSearch(),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, authData, _) => MaterialApp(
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          debugShowCheckedModeBanner: false,
          title: 'Vrea',
          theme: ThemeData(
            fontFamily: 'Montserrat',
            textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
            primaryColor: Hexcolor("#311B92"),
            focusColor: Hexcolor("#311B92"),
            visualDensity: VisualDensity.adaptivePlatformDensity,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
          ),
          home: OnBoardingScreen(),
          initialRoute: '/',
          routes: {
            LoginScreen.routeName: (ctx) => LoginScreen(),
            MainScreen.routeName: (ctx) => MainScreen(),
            ProfileSettingsScreen.routeName: (ctx) => ProfileSettingsScreen(),
            WorkerSearchScreen.routeName: (ctx) => WorkerSearchScreen(),
          },
        ),
      ),
    );
  }
}
