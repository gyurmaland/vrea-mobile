import 'package:vrea/models/interval.dart';
import 'package:vrea/service/api_base_helper.dart';

class IntervalRepository {
  ApiBaseHelper _helper = ApiBaseHelper();

  Future<List<IntervalModel>> fetchIntervalList(String workDay) async {
    var intervalRequest = {
      "dateTime": workDay,
    };
    final response = await _helper.post("/intervals/selected-date", intervalRequest);
    return IntervalsResponse.fromJson(response).results;
  }
}
