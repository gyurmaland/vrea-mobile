import 'dart:async';

import 'package:vrea/models/interval.dart';
import 'package:vrea/repository/interval_repository.dart';
import 'package:vrea/service/api_response.dart';

class IntervalBloc {

  IntervalRepository _intervalRepository;
  StreamController _intervalListController;
  StreamSink<ApiResponse<List<IntervalModel>>> get intervalListSink =>
      _intervalListController.sink;

  Stream<ApiResponse<List<IntervalModel>>> get intervalListStream =>
      _intervalListController.stream;

  IntervalBloc(String workDay) {
    _intervalListController = StreamController<ApiResponse<List<IntervalModel>>>();
    _intervalRepository = IntervalRepository();
    fetchIntervalList(workDay);
  }
  fetchIntervalList(String workDay) async {
    intervalListSink.add(ApiResponse.loading('Fetching Intervals'));
    try {
      List<IntervalModel> intervals = await _intervalRepository.fetchIntervalList(workDay);
      intervalListSink.add(ApiResponse.completed(intervals));
    } catch (e) {
      intervalListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }
  dispose() {
    _intervalListController?.close();
  }
}