import 'package:flutter/material.dart';

// API constants
const apiBaseURL = "https://vrea.herokuapp.com";

// Style constants
const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);

const kDefaultPadding = 20.0;