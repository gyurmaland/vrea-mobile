import 'dart:convert';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import 'package:vrea/models/worker.dart';
import 'package:vrea/constants.dart';
import 'package:vrea/providers/worker_search.dart';

class WorkerRemoteApi {
  static final _storage = FlutterSecureStorage();

  static Future<List<Worker>> postWorkerList(
          int pageKey, int pageSize, WorkerSearch workerSearch) async =>
      await _storage.read(key: 'authToken').then(
            (authToken) => http
                .post(
                  '$apiBaseURL/workers',
                  headers: {
                    HttpHeaders.authorizationHeader: 'Bearer $authToken',
                    HttpHeaders.contentTypeHeader: 'application/json',
                  },
                  body: json.encode(
                    {
                      'pagingRequest': {
                        'pageSize': pageSize,
                        'page': pageKey,
                      },
                      'userSearchRequest': {
                        "skill": null,
                        "location": {"lat": 47.9002494, "lng": 20.3776653},
                        "userListOrderBy": "DISTANCE_FROM_CURRENT_LOCATION",
                        "workDateTime": workerSearch.getWorkDateTime(),
                      }
                    },
                  ),
                )
                .mapFromResponse(
                  (jsonArray) => _parseItemListFromJsonArray(
                    jsonArray,
                    (jsonObject) => Worker.fromJson(jsonObject),
                  ),
                ),
          );

  static List<T> _parseItemListFromJsonArray<T>(
    List<dynamic> jsonArray,
    T Function(dynamic object) mapper,
  ) =>
      jsonArray.map(mapper).toList();
}

class GenericHttpException implements Exception {}

class NoConnectionException implements Exception {}

extension on Future<http.Response> {
  Future<R> mapFromResponse<R, T>(R Function(T) jsonParser) async {
    try {
      final response = await this;
      if (response.statusCode == 200) {
        return jsonParser(
            json.decode(utf8.decode(response.bodyBytes))['content']);
      } else {
        throw GenericHttpException();
      }
    } on SocketException {
      throw NoConnectionException();
    } catch (error) {
      print(error);
      throw (error);
    }
  }
}
