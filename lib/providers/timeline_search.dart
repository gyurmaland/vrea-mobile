import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';

class TimeLineSearch with ChangeNotifier {
  DateTime selectedWorkDay = DateTime.now();

  void updateTimelineWorkDay(DateTime newValue) {
    selectedWorkDay = newValue;
    notifyListeners();
  }

  String getTimeLineWorkDay() {
    if (selectedWorkDay != null) {
      return DateFormat('yyyy-MM-dd').format(selectedWorkDay);
    }
    return null;
  }
}
