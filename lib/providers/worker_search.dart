import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';

enum Skill { GARDENING, BABYSITTING, DRIVING }

enum UserListOrderBy { ID, DISTANCE_FROM_CURRENT_LOCATION }

class WorkerSearch with ChangeNotifier {
  Skill skill;
  LatLng location;
  DateTime workDateTime;

  void updateWorkDateTime(DateTime newValue) {
    workDateTime = newValue;
    notifyListeners();
  }

  String getWorkDateTime() {
    if (workDateTime != null) {
      return DateFormat('yyyy-MM-dd').format(workDateTime);
    }
    return null;
  }
}

class LatLng {
  final double latitude;
  final double longitude;

  LatLng(this.latitude, this.longitude);
}
