import 'dart:async';

import 'package:flutter/widgets.dart';

import 'package:flutter_web_auth/flutter_web_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:vrea/constants.dart';

class Auth with ChangeNotifier {
  String _userId;
  final _storage = FlutterSecureStorage();

  String get userId {
    return _userId;
  }

  Future<void> googleAuthenticate() async {
    final result = await FlutterWebAuth.authenticate(
        url:
            "$apiBaseURL/oauth2/authorize/google?redirect_uri=vrea://oauth2/redirect",
        callbackUrlScheme: "vrea");

    await _storage.write(
        key: 'authToken', value: Uri.parse(result).queryParameters['token']);
  }
}
