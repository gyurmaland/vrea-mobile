import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import 'package:vrea/constants.dart';

class UserData {
  final int id;
  final String name;
  final String imageUrl;

  UserData({
    this.id,
    this.name,
    this.imageUrl,
  });
}

class User with ChangeNotifier {
  UserData _user;

  UserData get user {
    return _user;
  }

  //TODO: kiszervezni majd a tokenezést
  Future<void> fetchAndSetProfileData() async {
    final url = '$apiBaseURL/user/me';
    final _storage = FlutterSecureStorage();
    String authToken = await _storage.read(key: 'authToken');
    try {
      final response = await http.get(
        url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer $authToken'},
      );
      final extractedData =
          json.decode(utf8.decode(response.bodyBytes)) as Map<String, dynamic>;
      if (extractedData == null) {
        return;
      }
      final UserData userData = UserData(
        id: extractedData['id'],
        name: extractedData['name'],
        imageUrl: extractedData['imageUrl'],
      );
      _user = userData;
      notifyListeners();
    } catch (error) {
      print(error);
      throw (error);
    }
  }
}
